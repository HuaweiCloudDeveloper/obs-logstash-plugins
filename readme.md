### 背景

Logstash是一个开源的服务器端数据处理管道，支持各种输入选择，可以同时从多个数据源获取数据，可以在同一时间从众多常用来源捕捉事件。能够以连续的流式传输方式，轻松地从您的日志、指标、Web 应用、数据存储等采集数据，并对其进行转换，然后将其发送到目标存储。

### 项目简介

利用Logstash的数据源扩展机制，使Logstash可以使用obs作为输入，输出数据源，包含但不限于提供功能文档，使用文档，代码贡献到Logstash仓库等等

### 前提条件

logstash版本 ≥ 7.10.2

### 部署步骤

以logstash-7.10.2为例。

1.下载logstash-7.10.2-linux-x86_64.tar.gz，并解压到/home/logstash目录。

```sh
wget https://artifacts.elastic.co/downloads/logstash/logstash-7.10.2-linux-x86_64.tar.gz
tar -xzvf logstash-7.10.2-linux-x86_64.tar.gz
mv logstash-7.10.2 logstash
```

2.输出数据到OBS。

示例：以file为源端，以OBS为目的端。

创建配置文件logstash.yml。详细参数说明见表1。

```
input {
  file {
         path => "/var/log/cloud-init.log"
    start_position => "beginning"
  }
}

filter {
    
}

output {
  s3 {
    	endpoint  => "obs endpoint"
    	bucket => "obs桶名"
    	access_key_id => "ak"
    	secret_access_key => "sk"
    	size_file => 1048576
        time_file => 1
        prefix => "logstash/"
        enable_metric => true
  }
}
```

| 参数              | 说明                                                         |
| ----------------- | ------------------------------------------------------------ |
| endpoint          | OBS的endpoint，必须包含https://前缀，例如https://obs.cn-south-1.myhuaweicloud.com |
| access_key_id     | 具备访问OBS权限的ak。                                        |
| secret_access_key | 具备访问OBS权限的sk。                                        |
| bucket            | OBS的桶名称。                                                |
| size_file         | 指定文件滚动大小（字节）。当文件大小达到设定的值时，会生成一个新的文件。 |
| time_file         | 设置文件滚动周期（分钟）。当数据写入达到设定周期时，会生成一个新的文件。 |
| prefix            | 指定文件存储的目录，例如“logstash/”，此时文件会写入到桶的logstash/目录下（注意路径不要以/开头）。 |

3.执行以下命令，运行logstash。

```sh
cd /home/logstash
bin/logstash -f logstash.yml
```

参考资料

- [OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1507.html)
- [Logstash input-plugins](https://www.elastic.co/guide/en/logstash/current/output-plugins.html)
- [Logstash output-plugins](https://www.elastic.co/guide/en/logstash/current/output-plugins.html) 